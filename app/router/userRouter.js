const express=require('express');
const router=express.Router();

const {
    createUserMiddleware,
    getAllUserMiddleware,
    getUserByIdMiddleware,
    updateUserMiddleware,
    deleteUserMiddleware
}=require("../middlewares/userMiddleware");

const{
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById
}=require("../controllers/userController");

router.post("/users",createUserMiddleware,createUser);
router.get("/users",getAllUserMiddleware,getAllUser);
router.get("/users/:userId",getUserByIdMiddleware,getUserById);
router.put("/users/:userId",updateUserMiddleware,updateUserById);
router.delete("/users/:userId",deleteUserMiddleware,deleteUserById);
module.exports=router;