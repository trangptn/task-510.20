const mongoose=require('mongoose');
const userModel=require('../models/userModel');

const createUser = ( req , res ) => {
    //B1:Thu thap du lieu
    const{fullName,
        email,
        address,
        phone}=req.body;
    //B2:Validate data
    if( !fullName){
        return res.status(400).json({
            message: " fullName is required!"
        })
    }
    if(!email)
    {
        return res.status(400).json({
            message: " email is required"
        })
    }
    if(!address){
        return res.status(400).json({
            message: " Adress is required"
        })
    }
    if(!phone)
    {
        return res.status(400).json({
            message: " phone is required"
        })
    }
    //B3:Xu ly va tra ve ket qua
    let newUser= new userModel({
        _id: new mongoose.Types.ObjectId(),
        fullName,
        email,
        address,
        phone,
    })

    userModel.create(newUser)
    .then(result=>{
        res.status(201).json({
            message:"Successfully created",
            data: result
        })
    })
    .catch(err=>{
        res.status(500).json({
            message:`Internal sever error: ${err.message}`
        })
    })
}

const getAllUser=(req,res) =>{
    //B1: Thu thap du lieu
    //B2:validate data
    //B3: Xu ly va tra ve ket qua
    userModel.find().exec()
        .then(data=> {
            return res.status(200).json({
                message:"Successfully load all data!",
                user: data
            })
        })
        .catch(error =>{
            return res.status(500).json({
                message:`Internal sever error: ${error.message}`
            })
        })
}

const getUserById=(req,res)=>{
     //B1: Thu thap du lieu
     var userId=req.params.userId;
    //B2:validate data
    if(!mongoose.Types.ObjectId.isValid(userId))
    {
        return res.status(400).json({
            message:"Id is invalid!"
        })
    }
    //B3: Xu ly va tra ve ket qua
    userModel.findById(userId)
        .then(data=>{
            if(!data)
            {
                return res.status(404).json({
                    message:" User not found"
                })
            }
           return res.status(200).json({
            message:"Successfully load data by ID!",
            user: data
           })
        })
        .catch(err=>{
            return res.status(500).json({
                message:`Internal sever error: ${err.message}`
            })
        })
}

const updateUserById=(req,res)=>{
     //B1: Thu thap du lieu
     var userId=req.params.userId;
     const{fullName,
        email,
        address,
        phone}=req.body;
    //B2:validate data
    if(!mongoose.Types.ObjectId.isValid(userId))
    {
        return res.status(400).json({
            message:"Id is invalid!"
        })
    }
    if( !fullName){
        return res.status(400).json({
            message: " fullName is required!"
        })
    }
    if(!email)
    {
        return res.status(400).json({
            message: " email is required"
        })
    }
    if(!address){
        return res.status(400).json({
            message: " Adress is required"
        })
    }
    if(!phone)
    {
        return res.status(400).json({
            message: " phone is required"
        })
    }
    //B3: Xu ly va tra ve ket qua
    var newUpdateUser= {};
    if(fullName) {
        newUpdateUser.fullName = fullName
    }
    if(email) {
        newUpdateUser.email = email
    }
    if(address) {
        newUpdateUser.address = address
    }
    if(phone) {
        newUpdateUser.phone = phone
    }
    userModel.findByIdAndUpdate(userId,newUpdateUser)
        .then(data=>{
            if(!data)
            {
                return res.status(404).json({
                    message:" User not found"
                })
            }
           return res.status(200).json({
            message:"Successfully update data by ID!",
            user: data
           })
        })
        .catch(err=>{
            return res.status(500).json({
                message:`Internal sever error: ${err.message}`
            })
        })
}

const deleteUserById=(req,res)=>{
     // B1: Thu thap du lieu
     const userId = req.params.userId;

     // B2: Validate du lieu
     if(!mongoose.Types.ObjectId.isValid(userId)) {
         return res.status(400).json({
             message: "Course ID khong hop le"
         })
     }
 
     // B3: Xu ly du lieu
     userModel.findByIdAndRemove(userId)
        .then(data=>{
            if(!data)
            {
                return res.status(404).json({
                    message:" User not found"
                })
            }
           return res.status(200).json({
            message:"Successfully delete data by ID!",
           })
        })
        .catch(err=>{
            return res.status(500).json({
                message:`Internal sever error: ${err.message}`
            })
        })
}
module.exports={
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById
}